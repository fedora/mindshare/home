Fedora Mindshare Committee
==========================

This repository is the project management and task tracker tool for the Fedora Mindshare Committee.
Ongoing work that need a resolution are tracked as **Issues** in this repository.
You can make a request to the Fedora Mindshare Committee here (issue templates are there to guide you).
Tickets are normally discussed during the Committee's regular meetings.


## [_Open a new issue here_](https://gitlab.com/fedora/mindshare/home/-/issues/new)


## What is the Fedora Mindshare Committee?

The _Fedora Mindshare Committee_ represents the outreach leadership in Fedora.
Mindshare aims to help outreach teams to reach their targets in a more effective way, by unifying and sharing their working process through an optimized and standardized communication.
It consists of mostly appointed, but also elected members.
You can read more about the Fedora Mindshare Committee in the [Fedora documentation](https://docs.fedoraproject.org/en-US/mindshare-committee/).


## How to contact the Mindshare Committee

The Fedora Mindshare Committee uses the following communication platforms:

* _Asynchronous_:
  [discussion.fedoraproject.org/tag/mindshare](https://discussion.fedoraproject.org/tag/mindshare)
* _Synchronous_:
  [matrix.to/#/#mindshare:fedoraproject.org](https://matrix.to/#/#mindshare:fedoraproject.org)

The Discourse forum is best for _asynchronous_ communication.
This means it is best for questions or topics that someone may respond to later.
It is better for longer, threaded discussions.

The Matrix room is best for _synchronous_ communication.
This means it is best for quick feedback, like a conversation.
It is helpful for real-time discussions or getting someone's attention more quickly.
For convenience, the Matrix room is also bridged to an IRC channel on the LiberaChat network:

* _LiberaChat IRC web chat_: [#fedora-mindshare](https://web.libera.chat/?channels=#fedora-mindshare)
