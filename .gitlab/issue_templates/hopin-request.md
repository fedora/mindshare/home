# Hopin Admin Request Template

<!---This template can be used by anyone who wants to request to be Hopin Admin on Fedora Project's behalf (should be related to Fedora project use cases). Vote of the Fedora Community Architect is compulsory for this ticket.

Items marked with asterisk (*) are mandatory fields. Use bullet points and simple language for a faster response time.--->

1. Primary Event Manager*
2. Additional Event Managers*
3. Event Name*
4. Brief Description of the Event*
5. Date(s) + Time(s) of the Event*
6. Estimated registrations for the event*
7. How many members of your planning team will need to be coded as organizers in the system pre-event?
NOTE: Hopin defines organizers are those people who are working on the back-end to set-up the event.  
8. How many members of your team will need to be coded as organizers in the system during the event?
HOPIN: Organizers are also those who have administrator access on the day of the event to help moderate and troubleshoot.
9. Are you using Hopin for your event registration? If no, what platform are you using for registration?
10. Will you be uploading event contacts or leads into a CRM? Yes/No
11. Would you like to contact event attendees after the event? If so, when and what information will you be sharing?
