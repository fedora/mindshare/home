<!--Thank you for your interest in proposing an event for the Fedora community. This issue template is the first place to start in getting support and/or funding for your event. The Fedora Mindshare Committee is responsible for reviewing, discussing, and voting on your event.

This issue template is specifically for MEDIUM and LARGE EVENTS, as documented in the Mindshare Committee event policies:

- https://docs.fedoraproject.org/en-US/mindshare-committee/events/requests/medium/
- https://docs.fedoraproject.org/en-US/mindshare-committee/events/requests/large/

This GitLab issue is the primary place to get feedback and requests on your event proposal. The issue is meant to be public, to allow for community feedback and participation.-->

## About you

<!--These questions help us understand more about you and your participation with Fedora.-->

* **Name**:

* **FAS username**:

* **How do you primarily contribute to Fedora?** (2-3 sentences):


## About the event

<!--These questions help us understand more about the proposed event.-->

* **Event name(s)**:

* **Event dates**<!--e.g. from 28 February 2027 to 2 March 2027-->:

* **Event location(s)**<!--city, state/province/county, country-->:

* **Event URL(s)**:

* **Event description(s)** (2-4 sentences per event):


## Requested support

<!--These questions help the Mindshare Committee understand what support you need to be a responsible representative of the Fedora community.-->

_NOTE_: The budget amount you request is a hard limit. If actual spend exceeds the requested budget, the extra amount will not be reimbursed. It is suggested to pad your requested budget with a buffer amount, in case of changing travel costs, hotel rates, etc.

* **Support requested**<!--Delete all that do NOT apply.-->: hotel | travel—air | travel—taxi/ride-share/parking | travel—rail | visa application support

* **Budget estimates**: <!--Make estimates in US Dollar (USD) to the best of your ability. Use a site like Skyscanner.com to estimate airfare.-->
    * **Travel expenses**<!--In USD: Air, rail, taxi/ride-share/parking-->:
    * **Hotel expenses**<!--In USD: Hotel or hostel. Note, Airbnb's are NOT allowed and will NOT be reimbursed.-->:

* **Accessibility needs & requests**<!--If you require special accommodations, describe these needs so they can be prioritized early.-->:

* **Other needs**<!--Anything else you must have to be successful that is not listed above.-->:


## This event & Fedora

<!--Fedora gets many requests for events. These questions help us understand whether the Fedora Project is a good fit for the event and the interests of the Fedora community.-->

* **Are you a speaker or presenter? If yes, give the title and description of your presentation**:

* **Are you an organizer or volunteer? If yes, explain your role**:

* **Why is it a good idea for the Fedora Project to have a presence?**:

* **Why do you want to represent the Fedora community here?**:

* **Are there others in the Fedora community who could help this event be successful? If yes, name any key individuals, teams, or SIGs in the Fedora community needed for success**:


## Other

* **Is there anything else the Fedora Mindshare Committee should know when reviewing your request?**:


<!--DO NOT EDIT BELOW THIS LINE!-->

/labels ~"?::needs committee vote" ~"category::physical events" ~"team::ambassadors"
