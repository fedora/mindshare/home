# Swag Only Request Template

This template can be used by anybody who just needs swag for event. The requests are monitored and acted upon by mindshare.

- All fields are required.

1. Full Name
2. FAS ID
3. Target Audience/Goal
4. Event Date
5. Event Location
6. Estimated Number of People
7. Swag Needed/Suggested
