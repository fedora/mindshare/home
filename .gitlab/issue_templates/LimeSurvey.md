# Survey Request Template

This template can be used by anyone who wants to request a survey on Fedora Project's instance LimeSurvey (should be related to Fedora project use cases).
The requests are monitored and acted upon by mindshare. Vote of the FCAIC is compulsory for this ticket.
Once approved, please share a survey structure file (.lss) exported from the your personal LimeSurvey account (to know how: visit https://manual.limesurvey.org/Display/Export_survey)

Items marked with asterisk (*) are mandatory fields.  These tickets are
designed to be easy to do.  Therefore feel free to use bullet points
and simple language.

1. Full Name
2. FAS ID*
3. Start Date*
4. End date*
5. Motivation*
6. Pagure ticket, Wiki Page or related event*
7. Target Audience*
8. Survey Name*
9. Base Language (Default English)
10. Welcome Message*
11. Number of Questions*
12. Questions with question types, the types available on LimeSurvey are [here](https://manual.limesurvey.org/Question_types)
