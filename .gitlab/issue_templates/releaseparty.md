# Release Party Template

This template can be used by anybody who is holding a Release Party to apply for budgets and swags for their events. The requests are monitored and acted upon by mindshare.

- Marked with asterisk are mandatory fields
- An example wiki template will be https://fedoraproject.org/wiki/Release_Party_F29_Bangalore_India[this]

1. Full Name
2. FAS ID* 
3. Event Date* 
4. Event Location*
5. Wiki Page
6. Swags
7. Budget
8. Target Audience*
